FROM jenkins/jenkins:2.73.3-alpine
# don't run the interactive setup wizard
ENV JAVA_OPTS="-Djenkins.install.runSetupWizard=false"
USER root
RUN apk update     \
 && apk add docker \
 && apk add pytest \
 && apk add py-pip \
 && apk add sudo \
 && pip install --upgrade pip \
 && pip install docker-compose \
 && pip install pytest-cov \
 && pip install epydoc \
 && echo 'jenkins ALL = NOPASSWD: /var/jenkins_home/*' >> /etc/sudoers
COPY plugins.txt /usr/share/jenkins/ref/plugins.txt
RUN /usr/local/bin/install-plugins.sh $(cat /usr/share/jenkins/ref/plugins.txt | tr -d '\r' | tr '\n' ' ')
#COPY jobs /usr/share/jenkins/ref/jobs
RUN chmod 777 -R /usr/share/jenkins/ref
RUN chmod 777 -R /var/jenkins_home

